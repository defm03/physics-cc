from numpy import *
import pylab as p
import mpl_toolkits.mplot3d.axes3d as p3

# Doing something from cookbook ~ quite a lot of fun.
# http://www.scipy.org/Cookbook/Matplotlib/mplot3D

# u, v - parametric varibles 
# u - array from 0 to 2*pi; 100 elements
u=r_[0:2*pi:100j]
# v - array from 0 to 2*pi; 100 elements
v=r_[0:pi:100j]

# x,y,z - coordinates of points for plotting
# each one in a 100x100 array
x=10*outer(cos(u),sin(v))
y=10*outer(sin(u),sin(v))
z=10*outer(ones(size(u)),cos(v))


fig=p.figure()
ax = p3.Axes3D(fig)
ax.plot_wireframe(x,y,z)
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
p.show()

