#!/usr/bin/env python

# Testing out matplotlib; full manual here:
# http://www.scipy.org/Cookbook/Matplotlib/MulticoloredLine

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from matplotlib.colors import ListedColormap, BoundaryNorm

x = np.linspace(0,9 * np.pi, 250) 
y = np.sin(x)
z = np.cos(0.5 * (x[:-1] + x[1:]))

# Colormap for red, green and blue and a norm to color
cmap = ListedColormap(['r','g','b'])
norm = BoundaryNorm([-1, -0.5, 0.5, 1], cmap.N)

# Creates points like N x 1 x 2 arrays to divide in segments
# segment array for line collection need to be numlines x points
# per line x 2 (x and y)
points = np.array([x,y]).T.reshape(-1, 1, 2)
segments = np.concatenate([points[:-1], points[1:]], axis=1)

# Line collection object; setting the colormapping parameters
lc = LineCollection(segments, cmap=cmap, norm=norm)
lc.set_array(z)
lc.set_linewidth(3)
plt.gca().add_collection(lc)

plt.xlim(x.min(), x.max())
plt.ylim(-1.1, 1.1)
plt.show()

