# Testing second part of Multicolored Line tutorial in matplotlib:
# http://www.scipy.org/Cookbook/Matplotlib/MulticoloredLine

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection

t = np.linspace(0, 10, 200)
x = np.cos(np.pi * t)
y = np.sin(t)

# Dividing into segment like in first one ('multicoloredline-1')
points = np.array([x, y]).T.reshape(-1, 1, 2)
segments = np.concatenate([points[:-1], points[1:]], axis=1)

# Line collection object, setting the colormapping parameters.
lc = LineCollection(segments, cmap=plt.get_cmap('copper'),
    norm=plt.Normalize(0,10))
lc.set_array(t)
lc.set_linewidth(3)

plt.gca().add_collection(lc)
plt.xlim(-1, 1)
plt.ylim(-1, 1)
plt.show()


